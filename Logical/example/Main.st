
PROGRAM _INIT

	iol_call_0.MasterInterface := MASTER_INTERFACE;
	iol_call_0.PnDeviceName := PN_DEVICE_NAME;
	iol_call_0.Slot := 2;
	iol_call_0.SubSlot := 1;
	iol_call_0.Index := 102;
	iol_call_0.SubIndex := 0;
	iol_call_0.DataLength := SIZEOF(var_INT);
//	iol_call_0.DataLength := SIZEOF(var_DINT);
//	iol_call_0.DataLength := SIZEOF(array1);
	
END_PROGRAM

PROGRAM _CYCLIC

	iol_call_0.pData := ADR(var_INT);
//	iol_call_0.pData := ADR(var_DINT);
//	iol_call_0.pData := ADR(array1);
	iol_call_0();
	
	dBuffer ACCESS iol_call_0.pBuffer;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

