
{REDUND_ERROR} FUNCTION_BLOCK iol_call (*IO-Link over Profinet - iol_call protocol implementation*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Execute : BOOL;
		MasterInterface : STRING[80];
		PnDeviceName : STRING[80];
		Write : {REDUND_UNREPLICABLE} BOOL; (*0=read access, 1 = write access*)
		Slot : UINT;
		SubSlot : UINT;
		Index : {REDUND_UNREPLICABLE} INT; (*IO-Link index which has to be read*)
		SubIndex : {REDUND_UNREPLICABLE} INT;
		DataLength : INT; (*size of pData*)
		pData : {REDUND_UNREPLICABLE} UDINT;
	END_VAR
	VAR_OUTPUT
		Status : DINT;
		Length : INT; (*length of read IO-Link data*)
	END_VAR
	VAR
		GetHandle_0 : nxpnmGetHandle;
		AcyclicRead_0 : nxpnmAcyclicRead;
		AcyclicWrite_0 : nxpnmAcyclicWrite;
		step : INT;
		handle : UDINT;
		oldExecute : BOOL;
		pBuffer : UDINT;
	END_VAR
END_FUNCTION_BLOCK
