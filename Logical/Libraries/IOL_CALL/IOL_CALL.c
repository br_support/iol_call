
#include <bur/plctypes.h>
#include <string.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "iol_call.h"
#ifdef __cplusplus
	};
#endif

#define SwapTwoBytes(data) \
( (((data) >> 8) & 0x00FF) | (((data) << 8) & 0xFF00) ) 

#define SwapFourBytes(data)   \
( (((data) >> 24) & 0x000000FF) | (((data) >>  8) & 0x0000FF00) | \
  (((data) <<  8) & 0x00FF0000) | (((data) << 24) & 0xFF000000) ) 

struct ReadHeader {
	USINT function;
	USINT port;
	UINT fi_index;
	USINT status;
	UINT iol_index;
	USINT iol_subindex;
}__attribute__((packed));

struct WriteHeader {
	USINT function;
	USINT port;
	UINT fi_index;
	USINT control;
	UINT iol_index;
	USINT iol_subindex;
}__attribute__((packed));


/* IO-Link over Profinet - iol_call protocol implementation */
void iol_call(struct iol_call* inst)
{
	switch (inst->step)
	{
		case 0:
			inst->handle = 0xFFFFFFFF;
			inst->Status = TMP_alloc(BUFFER_SIZE,(void**)&inst->pBuffer); 
			if (inst->Status == 0)	{
				inst->step++;
			}
			break;
		
		case 1:
			inst->GetHandle_0.enable = 1;
			inst->GetHandle_0.pDevice = (UDINT)&inst->MasterInterface;
			inst->GetHandle_0.pName = (UDINT)&inst->PnDeviceName;
			
			nxpnmGetHandle(&inst->GetHandle_0);
			inst->Status = inst->GetHandle_0.status;
			if (inst->Status == 0)	{
				inst->handle = inst->GetHandle_0.handle;
				inst->step++;
			}
			break;

		case 2:
			inst->step = (inst->Write) ? 20:10;
			break;
		
		case 10: //wait state
			if (inst->Execute > inst->oldExecute) {
				inst->Status = -1;
				inst->step = 20;
			}
			break;
			
		case 20: //first write
			{
				struct WriteHeader *writeHeader = (struct WriteHeader *)inst->pBuffer;
				writeHeader->function = 8;	//Function, fixed
				writeHeader->port = 1;	//Port, where IO-Link device is plugged	
				writeHeader->fi_index = SwapTwoBytes(0xfe4a); //FI_Index, fixed
				writeHeader->control = (inst->Write)? 2:3;	//Control, 2 = Write, 3 = Read On-request Data
				writeHeader->iol_index = SwapTwoBytes(inst->Index);	//IOL_Index
				writeHeader->iol_subindex = inst->SubIndex;	//IOL_SubIndex	
			
				if (inst->Write) {
					inst->Length = inst->DataLength;
					void *destData = (void*)inst->pBuffer + sizeof(struct WriteHeader);	
					void *srcData = (void*)inst->pData;
					//byte swapping
					if (inst->DataLength == 4) {
						DINT data1 = *(DINT*)srcData;
						*((DINT*)destData) = SwapFourBytes(data1);
					} else if (inst->DataLength == 2) {
						INT data1 = *(INT*)srcData;
						*((INT*)destData) = SwapTwoBytes(data1);
					} else if (inst->DataLength <= BUFFER_SIZE - sizeof(struct WriteHeader)) {
						memcpy(destData, srcData, inst->DataLength);
					}	
				} else {
					inst->Length = 0;
				}
			
				inst->AcyclicWrite_0.enable = 1;
				inst->AcyclicWrite_0.pDevice = (UDINT)&inst->MasterInterface;
				inst->AcyclicWrite_0.api = 0;
				inst->AcyclicWrite_0.slot = inst->Slot;
				inst->AcyclicWrite_0.subslot = inst->SubSlot;
				inst->AcyclicWrite_0.index = 0x00FF;
				inst->AcyclicWrite_0.dataLength = sizeof(struct WriteHeader) + inst->Length;
				inst->AcyclicWrite_0.pData = inst->pBuffer;
			
				nxpnmAcyclicWrite(&inst->AcyclicWrite_0);
				if (inst->AcyclicWrite_0.status == 0) {
					inst->step++;
				}
				break;
			}
		case 21: //then read
			
			inst->AcyclicRead_0.enable = 1;
			inst->AcyclicRead_0.pDevice = (UDINT)&inst->MasterInterface;
			inst->AcyclicRead_0.api = 0;
			inst->AcyclicRead_0.slot = inst->Slot;
			inst->AcyclicRead_0.subslot = inst->SubSlot;
			inst->AcyclicRead_0.index = 0x00FF;
			inst->AcyclicRead_0.dataLengthMax = BUFFER_SIZE;
			inst->AcyclicRead_0.pData = inst->pBuffer;
				
			nxpnmAcyclicRead(&inst->AcyclicRead_0);
			if (inst->AcyclicRead_0.status == 0) {
				inst->step++;;
			}		
			break;
		
		case 22:
			{
				inst->Length = inst->AcyclicRead_0.dataLength - sizeof(struct ReadHeader);
				struct ReadHeader *readHeader = (struct ReadHeader *)inst->pBuffer;
				void *srcData = (void*)inst->pBuffer + sizeof(struct ReadHeader);	
				void *dstData = (void*)inst->pData;
				if (readHeader->status) { 
					//error
					DINT data1 = *(DINT*)srcData;
					inst->Status = SwapFourBytes(data1);
				} else {	
					if (!inst->Write) {
						memset(dstData,0,inst->DataLength);
						//byte swapping
						if (inst->DataLength == 4) {
							DINT data1 = *(DINT*)srcData;
							*((DINT*)dstData) = SwapFourBytes(data1);
						} else if (inst->DataLength == 2) {
							INT data1 = *(INT*)srcData;
							*((INT*)dstData) = SwapTwoBytes(data1);
						} else if (inst->Length <= inst->DataLength) {
							memcpy(dstData, srcData, inst->Length);		
						}
					}
					inst->Status = 0;
				}
				inst->step = 10;
				break;
			}
	}
	inst->oldExecute = inst->Execute; 
}
